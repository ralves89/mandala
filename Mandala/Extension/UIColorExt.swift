//
//  UIColorExt.swift
//  Mandala
//
//  Created by Renee Alves on 07/10/19.
//  Copyright © 2019 Renee Alves. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    class func random() -> UIColor {
        
        let hue:CGFloat = CGFloat(Float(arc4random()) / Float(UINT32_MAX))
        return UIColor(hue: hue, saturation: 0.8, brightness: 1.0, alpha: 0.8)
    }
    
    class func someBlack() -> UIColor {
        
        return UIColor(red: 33.0/255.0, green: 33.0/255.0, blue: 33.0/255.0, alpha: 1.0)
    }
}
