//
//  Common.swift
//  Mandala
//
//  Created by Renee Alves on 07/10/19.
//  Copyright © 2019 Renee Alves. All rights reserved.
//

import Foundation
import UIKit
import CoreGraphics

func translate(point: CGPoint, index: Int, lines: Int) -> CGAffineTransform {
    
    let angle = CGFloat(index) * CGFloat(.pi * 2.0) / CGFloat(lines)
    let rotation = CGAffineTransform(rotationAngle: angle)
    
    var transformation = CGAffineTransform(translationX: point.x, y: point.y)
    transformation = rotation.concatenating(transformation)
    transformation = transformation.translatedBy(x: -point.x, y: -point.y)
    
    return transformation
}

func createRect(with start: CGPoint, with end: CGPoint, inset: CGFloat) -> CGRect {
    
    let origin = CGPoint(x: min(start.x, end.x), y: min(start.y, end.y))
    let size = CGSize(width:abs(start.x - end.x), height:abs(start.y - end.y))

    return CGRect(origin:origin, size:size).insetBy(dx: -inset, dy: -inset)
}

func createContext(width: CGFloat, height: CGFloat, contentScaleFactor: CGFloat) -> CGContext? {
    let width = Int(ceil(width * contentScaleFactor))
    let height = Int(ceil(height * contentScaleFactor))
    let RGB = CGColorSpaceCreateDeviceRGB()
    let BGRA = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedFirst.rawValue | CGBitmapInfo.byteOrder32Little.rawValue)
    return CGContext(data: nil, width: width, height: height, bitsPerComponent: 8, bytesPerRow: width * 4, space: RGB, bitmapInfo: BGRA.rawValue)
}

func setup(context: inout CGContext, width: CGFloat, bounds: CGRect, contentScaleFactor: CGFloat) {
    
    context.setLineWidth(width)
    context.setLineCap(CGLineCap.round)
    context.setLineJoin(CGLineJoin.round)
    context.scaleBy(x: contentScaleFactor, y: contentScaleFactor)
    context.concatenate(CGAffineTransform(a: 1, b: 0, c: 0, d: -1, tx: 0, ty: bounds.size.height))
    let fillColor = UIColor.black
    context.setFillColor(fillColor.cgColor)
    context.setStrokeColor(UIColor.random().cgColor)
    context.fill(bounds)
}
