//
//  ViewController.swift
//  Mandala
//
//  Created by Renee Alves on 02/10/19.
//  Copyright © 2019 Renee Alves. All rights reserved.
//

import UIKit

enum ShapeType {
    
    case symmetricalStar
    case symmetricalLines
}

class ViewController: UIViewController {
    
    @IBOutlet weak var symmetricalLineButton: UIButton!
    @IBOutlet weak var symeetricalStarButton: UIButton!
    
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var reset: UIButton!
    var shapeType: ShapeType = .symmetricalLines
    var lastScale: CGFloat = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupResetButton()
        setupButtons()
        addSymmetricalView()
    }
    
    private func setupResetButton() {
        
        reset.addTarget(self, action: #selector(didTapReset(sender:)), for: .touchUpInside)
    }
}

extension ViewController {
    
    @objc func didTapReset(sender: UIButton) {
        
        clear()
        addSymmetricalView()
    }
    
    private func clear() {
        
        for subview in baseView.subviews {
            
            subview.removeFromSuperview()
        }
    }
    
    private func setupButtons() {
        
        symeetricalStarButton.addTarget(self, action: #selector(didTapSymmetricalStar), for: .touchUpInside)
        symmetricalLineButton.addTarget(self, action: #selector(didTapSymmetricalLine), for: .touchUpInside)
    }
    
    @objc func didTapSymmetricalStar() {
        
        shapeType = .symmetricalStar
        addSymmetricalView()
    }
    
    @objc func didTapSymmetricalLine() {
        
        shapeType = .symmetricalLines
        addSymmetricalView()
    }
    
    private func addSymmetricalView() {
        
        for subview in baseView.subviews {
            
            if subview.isKind(of: SymmetricalView.self) {
                
                guard let symmetricalView = subview as? SymmetricalView else {return}
                symmetricalView.shapeType = shapeType
                symmetricalView.initLines(type: shapeType)
                symmetricalView.setupContextColor()
                return
            }
        }
        
        let simmetricalView = SymmetricalView(frame: self.baseView.bounds, type: shapeType)
        simmetricalView.backgroundColor = baseView.backgroundColor
        baseView.addSubview(simmetricalView)
    }
}

