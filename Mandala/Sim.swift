//
//  Sim.swift
//  Mandala
//
//  Created by Renee Alves on 05/10/19.
//  Copyright © 2019 Renee Alves. All rights reserved.
//

import Foundation

import UIKit
import CoreGraphics

class SymmetricalView: UIView {

    var context: CGContext? = nil
    var lastPoint = CGPoint.zero
    let lineWidth: CGFloat = 9
    var lines: Int!
    var shapeType: ShapeType!
    
    init(frame: CGRect, type: ShapeType) {
        super.init(frame: frame)
        self.shapeType = type
        
        initLines(type: type)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initLines(type: ShapeType) {
        
        if type == .symmetricalLines {
            lines = 2
        } else {
            lines = 6
        }
    }
    
    func setupContext() {
        
        if context == nil {
            guard var newContext = createContext(width: self.bounds.size.width, height: self.bounds.size.height, contentScaleFactor: self.contentScaleFactor) else { return }
            
            setup(context: &newContext, width: lineWidth, bounds: self.bounds, contentScaleFactor: self.contentScaleFactor)
          
            self.context = newContext
        }
    }
    
    func clear() {
        setupContext()
        context!.fill(self.bounds)
        setNeedsDisplay()
    }
    
    func drawLine(startPoint: CGPoint, endPoint: CGPoint) {
        
        setupContext()
        let inset = ceil(lineWidth * 0.5)
        let center = CGPoint(x: self.bounds.midX, y: self.bounds.midY)
        
        for index in 0 ... lines {
            
            let transformation = translate(point: center, index: index, lines: lines)
            let start = startPoint.applying(transformation)
            let end = endPoint.applying(transformation)

            context?.move(to: CGPoint(x: start.x, y: start.y))
            context?.addLine(to: CGPoint(x: end.x, y: end.y))
            context?.strokePath()

            setNeedsDisplay(createRect(with: start, with: end, inset: inset))
        }
    }
  
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        lastPoint = touches.first!.location(in: self)
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        let point = touches.first!.location(in: self)
        drawLine(startPoint: lastPoint, endPoint: point)
        lastPoint = point
    }
  
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        let point = touches.first!.location(in: self)
        drawLine(startPoint: lastPoint, endPoint: point)
        setupContextColor()
    }
    
    func setupContextColor() {
        
        context?.setStrokeColor(UIColor.random().cgColor)
    }
  
    override func draw(_ rect: CGRect) {

        if context != nil {
            
            let viewContext = UIGraphicsGetCurrentContext()
            viewContext?.setBlendMode(.copy)
            viewContext?.interpolationQuality = .none

            guard let image = context!.makeImage() else { return }
            viewContext?.concatenate(CGAffineTransform(a: 1, b: 0, c: 0, d: -1, tx: 0, ty: self.bounds.size.height))
            viewContext?.draw(image, in: self.bounds)
        }
    }
}
